//
//  AlfaRssREaderTests.m
//  AlfaRssREaderTests
//
//  Created by Almir Akmalov on 28.01.17.
//  Copyright © 2017 Akm. All rights reserved.
//

#import <XCTest/XCTest.h>
#import "RequestManager.h"

static NSTimeInterval const timeoutTime = 120;

@interface AlfaRssREaderTests : XCTestCase

@end

@implementation AlfaRssREaderTests

- (void)setUp {
    [super setUp];
    // Put setup code here. This method is called before the invocation of each test method in the class.
}

- (void)tearDown {
    // Put teardown code here. This method is called after the invocation of each test method in the class.
    [super tearDown];
}

- (void)testExample {
    // This is an example of a functional test case.
    // Use XCTAssert and related functions to verify your tests produce the correct results.
}

- (void)testPerformanceExample {
    // This is an example of a performance test case.
    [self measureBlock:^{
        // Put the code you want to measure the time of here.
    }];
}

- (void) testXmlParseWithUrlLentaString {
    XCTestExpectation *expectation = [self expectationWithDescription:@"xmlParseWithUrlString"];
    
    
    [RequestManager.sharedManager xmlParseWithUrlString:@"https://alfabank.ru/_/rss/_rss.html?subtype=1&category=2&city=21" forced:NO complitionHandler:^(BOOL isReady, NSArray *noveltyItemsArray) {
    
        XCTAssertTrue(isReady);
        XCTAssertNotNil(noveltyItemsArray);
        
        [expectation fulfill];
    }];
    
    [self waitForExpectationsWithTimeout:timeoutTime handler:^(NSError *error) {
        if (error != nil) {
            NSLog(@"Error: %@", error.localizedDescription);
        }
    }];
    
}

@end
