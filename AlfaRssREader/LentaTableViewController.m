//
//  LentaTableViewController.m
//  AlfaRssREader
//
//  Created by Almir Akmalov on 29.01.17.
//  Copyright © 2017 Akm. All rights reserved.
//

#import "LentaTableViewController.h"
#import "RequestManager.h"
#import "NoveltyItem.h"

#import "NoveltyWebViewController.h"
//#import "NoveltyScrollCollectionViewController.h"

static NSString* const ALFAURL = @"https://alfabank.ru/_/rss/_rss.html?subtype=1&category=2&city=21";

@interface LentaTableViewController () <UIScrollViewDelegate>

@property (strong, nonatomic) IBOutlet UIView *emptyStateView;
@property (strong, nonatomic) IBOutlet UIView *loadingStateView;
@property (strong, nonatomic) IBOutlet UITableView *tableView;
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *activityIndicator;

@property (strong, nonatomic) NSArray *noveltyItemsArray;
@property (strong, nonatomic) UIRefreshControl *refreshControl;
@property (strong, nonatomic) NSIndexPath *selectedIndexPath;

@end

@implementation LentaTableViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.view = self.loadingStateView;
    [self.activityIndicator startAnimating];
    self.refreshControl = [[UIRefreshControl alloc] init];
    self.refreshControl.backgroundColor = [UIColor clearColor];
    self.refreshControl.tintColor = [UIColor grayColor];
    [self.tableView addSubview:self.refreshControl];
    
    __weak typeof(self) weakSelf = self;
    [RequestManager.sharedManager xmlParseWithUrlString:ALFAURL forced:NO complitionHandler:^(BOOL isReady, NSArray *noveltyItemsArray) {
        if (isReady) {
            weakSelf.noveltyItemsArray = noveltyItemsArray;
            [weakSelf showNoveltys];
        } else {
            [weakSelf showEmptyState];
        }
    }];
    
    [self performSelector:@selector(refreshTable) withObject:nil afterDelay:300];
}

- (void)refreshTable {
    
    __weak typeof(self) weakSelf = self;
    NSLog(@"refreshTable >> %@",[NSDate date]);
    [RequestManager.sharedManager xmlParseWithUrlString:ALFAURL forced:YES complitionHandler:^(BOOL isReady, NSArray *noveltyItemsArray) {
        if (isReady) {
            weakSelf.noveltyItemsArray = noveltyItemsArray;
            [weakSelf showNoveltys];
            if (!weakSelf.refreshControl.refreshing) {
                [weakSelf performSelector:@selector(refreshTable) withObject:nil afterDelay:300];
            }
            [weakSelf.refreshControl endRefreshing];
            [weakSelf.tableView reloadData];
        }
    }];
    
}


#pragma mark -

- (void) showEmptyState {
    self.view = self.emptyStateView;
    [self.activityIndicator stopAnimating];
}

- (void) showNoveltys {
    self.view = self.tableView;
    [self.activityIndicator stopAnimating];
    [self.tableView reloadData];
    
}


#pragma mark -

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.noveltyItemsArray.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"noveltyCell"];
    
    return cell;
}

- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath {
    NoveltyItem* item = [self.noveltyItemsArray objectAtIndex:indexPath.row];
    cell.textLabel.text = item.noveltyTitle;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    NoveltyItem* item = [self.noveltyItemsArray objectAtIndex:indexPath.row];
    NSString *text = item.noveltyTitle;
    CGRect rect = [text boundingRectWithSize:CGSizeMake(self.tableView.bounds.size.width, CGFLOAT_MAX)
                                     options:NSStringDrawingUsesLineFragmentOrigin
                                  attributes:@{NSFontAttributeName:[UIFont systemFontOfSize:17.f]}
                                     context:nil];
    
    if (rect.size.height < 44.f) {
        return 44.f;
    }
    return rect.size.height;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    self.selectedIndexPath = indexPath;
    [self performSegueWithIdentifier:@"Detail" sender:nil];
}

#pragma mark -

- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView {
    if (self.refreshControl.refreshing) {
        [self refreshTable];
    }
}


#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    NoveltyWebViewController *vc = segue.destinationViewController;
    vc.item = [self.noveltyItemsArray objectAtIndex:self.selectedIndexPath.row];
}


@end
