//
//  AboutMeTableViewController.m
//  AlfaRssREader
//
//  Created by Almir Akmalov on 30.01.17.
//  Copyright © 2017 Akm. All rights reserved.
//

#import "AboutMeTableViewController.h"

@interface AboutMeTableViewController ()
@property (weak, nonatomic) IBOutlet UILabel *aboutMeLabel;

@end

@implementation AboutMeTableViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
    
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.rightBarButtonItem = self.editButtonItem;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.row == 0) {
        return 150.f;
    } else {
        NSString *text = self.aboutMeLabel.text;
        CGRect rect = [text boundingRectWithSize:CGSizeMake(self.tableView.bounds.size.width - 30, CGFLOAT_MAX)
                                         options:NSStringDrawingUsesLineFragmentOrigin
                                      attributes:@{NSFontAttributeName:[UIFont systemFontOfSize:22.f]}
                                         context:nil];
        NSLog(@"rect.size.height  > %@",@(rect.size.height));
        if (rect.size.height < 44) {
            return 44;
        }
        return rect.size.height + 30;
    }
    
}

@end
