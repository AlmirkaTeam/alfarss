//
//  XmlDelegate.m
//  AlfaRssREader
//
//  Created by Almir Akmalov on 28.01.17.
//  Copyright © 2017 Akm. All rights reserved.
//

#import "XmlDelegate.h"
#import "NoveltyItem.h"

@interface XmlDelegate() <NSXMLParserDelegate> {
    NSString *_elementName;
    NSString *_title;
    NSString *_itemDescription;
    NSString *_pubDate;
    NSString *_url;
    NSData *_imageData;
    
}

@property (strong, nonatomic) NSXMLParser *parser;
@property (strong, nonatomic) NSMutableArray* itemsArray;
@property (strong, nonatomic) NSDateFormatter *dateFromatter;

@end

@implementation XmlDelegate

- (instancetype)initWithData:(NSData*)data {
    self = [super init];
    if (self) {
        _parser = [[NSXMLParser alloc] initWithData:data];
        
        [_parser setShouldProcessNamespaces:YES];
        [_parser setShouldReportNamespacePrefixes:YES];
        [_parser setShouldResolveExternalEntities:YES];
        [_parser setDelegate:self];
        _dateFromatter = [[NSDateFormatter alloc] init];
        [_dateFromatter setDateFormat:@"EEE, dd MMM yyyy HH:mm:ss ZZ"];
        _itemsArray = [NSMutableArray array];
    }
    return self;
}

- (void) transformXmlWithCompletionBlock:(void(^)(NSArray *result))completion {
    if ([self.parser parse]) {
        completion(self.itemsArray);
    }
}

#pragma mark - XMLParserDelegate

- (void) parser:(NSXMLParser *)parser didStartElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI qualifiedName:(NSString *)qName attributes:(NSDictionary *)attributeDict {
    
    _elementName = elementName;
    
    if ([_elementName isEqualToString:@"item"]) {
        
        _title = [NSString string];
        _itemDescription = [NSString string];
        _pubDate = [NSString string];
        _url = [NSString string];
        _imageData = [NSData data];
    } else if ([_elementName isEqualToString:@"enclosure"]) {
        NSString *imageUrl = [attributeDict objectForKey:@"url"];
        _imageData = [NSData dataWithContentsOfURL:[NSURL URLWithString:imageUrl]];
    }
}

-(void) parser:(NSXMLParser *)parser foundCharacters:(NSString *)string {
    
    NSString *trimmedString = [string stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
    if ([_elementName isEqualToString:@"title"]) {
        _title = [_title stringByAppendingString:trimmedString];
    } else if ([_elementName isEqualToString:@"description"]) {
        _itemDescription = [_itemDescription stringByAppendingString:string];
    } else if ([_elementName isEqualToString:@"pubDate"]) {
        _pubDate = [_pubDate stringByAppendingString:trimmedString];
    } else if ([_elementName isEqualToString:@"guid"]) {
        _url = [_url stringByAppendingString:trimmedString];
    }
}

- (void) parser:(NSXMLParser *)parser didEndElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI qualifiedName:(NSString *)qName {
    
    if ([elementName isEqualToString:@"item"]) {
        
        NoveltyItem *item = [[NoveltyItem alloc] initWithNoveltyTitle:_title
                                                   noveltyDescription:_itemDescription
                                                           noveltyUrl:_url
                                                              pubDate:[_dateFromatter dateFromString:_pubDate]
                                                     noveltyImageData:_imageData];
        [self.itemsArray addObject:item];
    }
}

@end
