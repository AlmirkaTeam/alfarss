//
//  NoveltyItem.m
//  AlfaRssREader
//
//  Created by Almir Akmalov on 28.01.17.
//  Copyright © 2017 Akm. All rights reserved.
//

#import "NoveltyItem.h"

@interface NoveltyItem ()

@property (strong, nonatomic) NSString* noveltyTitle;
@property (strong, nonatomic) NSString* noveltyDescription;
@property (strong, nonatomic) NSString* noveltyUrl;
@property (strong, nonatomic) NSDate* pubDate;
@property (strong, nonatomic) NSData* noveltyImageData;

@end

@implementation NoveltyItem

- (instancetype) initWithNoveltyTitle:(NSString*) noveltyTitle
                   noveltyDescription:(NSString*) noveltyDescription
                           noveltyUrl:(NSString*) noveltyUrl
                              pubDate:(NSDate*) pubDate
                     noveltyImageData:(NSData*) noveltyImageData
{
    self = [super init];
    if (self) {
        
        self.noveltyTitle = noveltyTitle;
        self.noveltyDescription = noveltyDescription;
        self.noveltyUrl = noveltyUrl;
        self.pubDate = pubDate;
        self.noveltyImageData = noveltyImageData;
    }
    return self;
}

@end
