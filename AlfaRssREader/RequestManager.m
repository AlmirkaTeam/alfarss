//
//  RequestManager.m
//  AlfaRssREader
//
//  Created by Almir Akmalov on 28.01.17.
//  Copyright © 2017 Akm. All rights reserved.
//

#import "RequestManager.h"
#import "XmlDelegate.h"


#define REQUEST_MANAGER_CACHE_PATH @"~/Library/Caches"

@interface RequestManager ()

@property (strong, nonatomic) NSURLSession* sharedSession;
@property (assign, nonatomic) NSTimeInterval cacheMaxAgeInSeconds;

@end

@implementation RequestManager

#pragma mark -

+ (RequestManager*) sharedManager {
    static RequestManager *manager = nil;
    
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        manager = [[RequestManager alloc] init];
        NSURLSession *session = nil;
        NSURLSessionConfiguration *sessionConfiguration = [NSURLSessionConfiguration defaultSessionConfiguration];
        [sessionConfiguration setAllowsCellularAccess:YES];
        [sessionConfiguration setRequestCachePolicy: NSURLRequestReloadIgnoringLocalCacheData];
        [sessionConfiguration setHTTPCookieAcceptPolicy:NSHTTPCookieAcceptPolicyNever];
        
        session = [NSURLSession sessionWithConfiguration:sessionConfiguration delegate:nil delegateQueue:nil];
        manager.sharedSession = session;
        manager.cacheMaxAgeInSeconds = 300; // 5 min
    });
    
    return manager;
}

#pragma mark -

- (void) xmlParseWithUrlString:(NSString*)urlString forced:(BOOL)forced complitionHandler:(RequestCompletion)completionHandler {
    
    NSURL* url = [NSURL  URLWithString:urlString];
    
    dispatch_queue_t requestQueue;
    requestQueue = dispatch_queue_create("rss.alfa.queue", NULL);
    dispatch_set_target_queue(requestQueue, dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0));
    
    dispatch_async(requestQueue, ^{
        if (![self tryTakeFromCacheUrl:url forced:forced withCompletionHandler:completionHandler] || forced) {
            NSURLSessionDataTask *task = [self.sharedSession dataTaskWithURL:url completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
                if ([response isKindOfClass:[NSHTTPURLResponse class]]) {
                    NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse *)response;
                    if (httpResponse.statusCode != 200) {
                        NSLog(@"Server returned non 200 code: %@", @(httpResponse.statusCode));
                        if (completionHandler) {
                            completionHandler(YES, nil);
                        }
                    }
                }
                
                if (error) {
                    NSLog(@"Error reading server's data: %@", error);
                    if (completionHandler) {
                        completionHandler(YES, nil);
                    }
                }
                
                if (data) {
                    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
                    NSString* cachePath = [self cachePathForUrl:url];
                    [userDefaults setObject:[NSDate date] forKey:[url host]];
                    [userDefaults synchronize];
                    NSError* error = nil;
                    BOOL res = [data writeToFile:cachePath options:NSDataWritingAtomic error:&error];
                    if (!res || error)
                        NSLog(@"Cache write failed to %@:\n%@", cachePath, error);
                    NSLog(@"xmlParseWithUrlString >> %@",[NSDate date]);
                    [self parseXmlWithData:data complitionHandler:completionHandler];
                }
            }];
            
            [task resume];
        }
    });
    
    
}

#pragma mark -

-(void)parseXmlWithData:(NSData *)data complitionHandler:(RequestCompletion)completionHandler{
    XmlDelegate * xmlDelegate = [[XmlDelegate alloc] initWithData:data];
    
    [xmlDelegate transformXmlWithCompletionBlock:^(NSArray *result) {
        dispatch_async(dispatch_get_main_queue(), ^{
            if (completionHandler) {
                NSLog(@"completionHandler >> %@",[NSDate date]);
                completionHandler(YES, result);
            }
        });
    }];
}

#pragma mark - Cache

-(NSString*)cachePathForUrl:(NSURL*)url {
    return [[NSString stringWithFormat:@"%@/%@.%@", REQUEST_MANAGER_CACHE_PATH, [NSBundle mainBundle].bundleIdentifier, [url host]] stringByExpandingTildeInPath];
}

- (BOOL)tryTakeFromCacheUrl:(NSURL*)url forced:(BOOL)forced withCompletionHandler:(RequestCompletion)handler {
    
    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    NSString* cachePath = [self cachePathForUrl:url];
    NSDate *last = [userDefaults objectForKey:[url host]];
    NSUInteger now = (NSUInteger)[[NSDate date] timeIntervalSince1970];
    BOOL fTakeFromCache = (last.timeIntervalSince1970 + self.cacheMaxAgeInSeconds > now);
    if (fTakeFromCache && !forced) {
        fTakeFromCache = NO;
        NSData* data = [NSData dataWithContentsOfFile:cachePath];
        // json check
        if (data) {
            fTakeFromCache = YES;
            // result
            NSLog(@"tryTakeFromCacheUrl >> %@",[NSDate date]);
            [self parseXmlWithData:data complitionHandler:handler];
            return YES;
        }
        
    }
    return NO;
}

@end
