//
//  NoveltyItem.h
//  AlfaRssREader
//
//  Created by Almir Akmalov on 28.01.17.
//  Copyright © 2017 Akm. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NoveltyItem : NSObject

@property (strong, nonatomic, readonly) NSString* noveltyTitle;
@property (strong, nonatomic, readonly) NSString* noveltyDescription;
@property (strong, nonatomic, readonly) NSString* noveltyUrl;
@property (strong, nonatomic, readonly) NSDate* pubDate;
@property (strong, nonatomic, readonly) NSData* noveltyImageData;

- (instancetype) initWithNoveltyTitle:(NSString*) noveltyTitle
                   noveltyDescription:(NSString*) noveltyDescription
                           noveltyUrl:(NSString*) noveltyUrl
                              pubDate:(NSDate*) pubDate
                     noveltyImageData:(NSData*) noveltyImageData;

@end
