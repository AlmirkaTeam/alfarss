//
//  XmlDelegate.h
//  AlfaRssREader
//
//  Created by Almir Akmalov on 28.01.17.
//  Copyright © 2017 Akm. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface XmlDelegate : NSObject

- (instancetype)initWithData:(NSData*)data;

- (void) transformXmlWithCompletionBlock:(void(^)(NSArray *result))completion;

@end
