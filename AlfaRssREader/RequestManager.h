//
//  RequestManager.h
//  AlfaRssREader
//
//  Created by Almir Akmalov on 28.01.17.
//  Copyright © 2017 Akm. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef void (^RequestCompletion)(BOOL isReady, NSArray * noveltyItemsArray);

@interface RequestManager : NSObject

@property (strong, nonatomic, readonly) NSURLSession* sharedSession;

+ (RequestManager*) sharedManager;

- (void) xmlParseWithUrlString:(NSString*)urlString forced:(BOOL)forced complitionHandler:(RequestCompletion)completionHandler;

@end
