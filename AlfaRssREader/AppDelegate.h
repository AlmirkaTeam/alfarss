//
//  AppDelegate.h
//  AlfaRssREader
//
//  Created by Almir Akmalov on 28.01.17.
//  Copyright © 2017 Akm. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <CoreData/CoreData.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@property (readonly, strong) NSPersistentContainer *persistentContainer;

- (void)saveContext;


@end

