//
//  NoveltyWebViewController.h
//  AlfaRssREader
//
//  Created by Almir Akmalov on 30.01.17.
//  Copyright © 2017 Akm. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "NoveltyItem.h"

@interface NoveltyWebViewController : UIViewController

@property (strong, nonatomic) NoveltyItem* item;

@end
