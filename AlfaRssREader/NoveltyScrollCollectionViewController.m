//
//  NoveltyScrollCollectionViewController.m
//  AlfaRssREader
//
//  Created by Almir Akmalov on 30.01.17.
//  Copyright © 2017 Akm. All rights reserved.
//

#import "NoveltyScrollCollectionViewController.h"
#import "WebViewCollectionViewCell.h"

@interface NoveltyScrollCollectionViewController ()

@end

@implementation NoveltyScrollCollectionViewController

static NSString * const reuseIdentifier = @"Cell";

- (void)viewDidLoad {
    [super viewDidLoad];
    
    // Uncomment the following line to preserve selection between presentations
    // self.clearsSelectionOnViewWillAppear = NO;
    
    // Register cell classes
    [self.collectionView registerClass:[WebViewCollectionViewCell class] forCellWithReuseIdentifier:reuseIdentifier];
    
    // Do any additional setup after loading the view.
}


#pragma mark <UICollectionViewDataSource>

- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView {

    return 1;
}


- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {

    return 1;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    WebViewCollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:reuseIdentifier forIndexPath:indexPath];
    
    UIWebView *wv = [[UIWebView alloc] init];
    
    cell.webView = wv;
    
    
    
    return cell;
}

- (void)collectionView:(UICollectionView *)collectionView willDisplayCell:(WebViewCollectionViewCell *)cell forItemAtIndexPath:(NSIndexPath *)indexPath {
    [cell.webView loadRequest:[NSURLRequest requestWithURL:[NSURL URLWithString:@"https://ya.ru"]]];
}

@end
