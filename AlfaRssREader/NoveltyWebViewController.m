//
//  NoveltyWebViewController.m
//  AlfaRssREader
//
//  Created by Almir Akmalov on 30.01.17.
//  Copyright © 2017 Akm. All rights reserved.
//

#import "NoveltyWebViewController.h"

@interface NoveltyWebViewController ()

@property (weak, nonatomic) IBOutlet UIWebView *noveltyWebView;

@end

@implementation NoveltyWebViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self.noveltyWebView loadRequest:[NSURLRequest requestWithURL:[NSURL URLWithString:self.item.noveltyUrl]]];
    
    UIBarButtonItem *barButton = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemAction target:self action:@selector(share)];
    self.navigationItem.rightBarButtonItem = barButton;
}

-(void)share {
    UIActivityViewController *activityController =
    [[UIActivityViewController alloc] initWithActivityItems:@[[NSURL URLWithString:self.item.noveltyUrl]]
                                      applicationActivities:nil];
    
    activityController.excludedActivityTypes = @[UIActivityTypePostToWeibo, UIActivityTypeAssignToContact, UIActivityTypeCopyToPasteboard, UIActivityTypeMessage,UIActivityTypeMail, UIActivityTypePrint, UIActivityTypeCopyToPasteboard, UIActivityTypeAssignToContact, UIActivityTypeSaveToCameraRoll, UIActivityTypeAddToReadingList, UIActivityTypePostToFlickr, UIActivityTypePostToVimeo, UIActivityTypePostToTencentWeibo, UIActivityTypeAirDrop, UIActivityTypeOpenInIBooks];
    
    [self presentViewController:activityController animated:YES completion:nil];
}

@end
